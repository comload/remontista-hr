#!/bin/env python
# -*- coding: utf-8 -*-

import json
import requests
from flask import Flask
from flask import request
from flask import jsonify
from cerberus import Validator
from datetime import datetime as dt

API_URL = "https://new58bee17a5ccea.amocrm.ru/private/api"
AUTH_URL = API_URL + "/auth.php?type=json"
CONTACTS_URL = API_URL + "/v2/json/contacts"

credents = dict(
    USER_HASH="665c7d9140b61be3d8f6c11cc4231bc9",
    USER_LOGIN="1337@0xdb.ru"
)

schema = {
    'request': {
        'type': 'dict',
        'schema': {
            'contacts': {
                'type': 'dict',
                'schema': {
                    'add': {
                        'type': 'list',
                        'schema': {
                            'type': 'dict',
                            'schema': {
                                'name': {
                                    'type': 'string',
                                    'required': True
                                },
                                'request_id': {
                                    'type': 'integer',
                                    'coerce': int
                                },
                                'date_create': {
                                    'type': 'integer',
                                    'coerce': int
                                },
                                'last_modified': {
                                    'type': 'integer',
                                    'coerce': int
                                },
                                'responsible_user_id': {
                                    'type': 'integer',
                                    'coerce': int
                                },
                                'linked_leads_id': {
                                    'type': 'list',
                                    'schema': {
                                        'type': 'integer',
                                        'coerce': int
                                    },
                                },
                                'company_name': {'type': 'string'},
                                'linked_company_id': {'type': 'string'},
                                'tags': {'type': 'string'},
                                'custom_fields': {
                                    'type': 'list',
                                    'schema': {
                                        'id': {
                                            'type': 'integer',
                                            'coerce': int
                                        },
                                        'values': {
                                            'type': 'list',
                                            'schema': {
                                                'type': 'dict',
                                                'schema': {
                                                    'value': {
                                                        'type': 'string'},
                                                    'enum': {
                                                        'type': 'string'},
                                                    'subtype': {
                                                        'type': 'string'}
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    'update': {
                        'type': 'list',
                        'schema': {
                            'type': 'dict',
                            'allow_unknown': True,
                            'schema': {
                                'id': {
                                    'type': 'integer',
                                    'coerce': int,
                                    'required': True
                                },
                                'last_modified': {
                                    'type': 'integer',
                                    'coerce': int,
                                    'required': True
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

query_schema = {
    'limit_rows': {'type': 'integer', 'coerce': int},
    'limit_offset': {'type': 'integer', 'coerce': int},
    'id': {'type': 'integer', 'coerce': int},
    'query': {'type': 'string'},
    'responsible_user_id': {
        'anyof': [
            {'type': 'integer', 'coerce': int},
            {'type': 'list', 'schema': {'type': 'integer', 'coerce': int}}
        ]
    },
    'type': {'type': 'string', 'allowed': ['contact', 'company', 'all']}
}

query_validator = Validator(query_schema)

validator = Validator(schema)

app = Flask(__name__)


def validate_request(request):
    if not validator.validate(request.json):
        return False, validator.errors
    return True, {}


def validate_list_query(request):
    if not query_validator.validate(request.args.to_dict()):
        return False, query_validator.errors
    return True, {}


def validate_modified(header):
    try:
        dt.strptime(header, "%a, %d %b %Y %H:%M:%S")
        return header
    except Exception:
        # print header
        return False


def auth():
    s = requests.Session()
    r = s.post(AUTH_URL, data=json.dumps(credents))
    if r.json()[u'response'][u'auth'] is not True:
        return None
    return s


@app.route("/contacts", methods=["GET", "POST"])
def contacts():
    s = auth()
    if s:
        if request.method == "POST":
            valid = validate_request(request)
            if not valid[0]:
                return jsonify(valid[1]), 400
            r = s.post(
                CONTACTS_URL + "/set",
                data=json.dumps(request.json)
            )
            if r.status_code == 200:
                return jsonify(r.json())
            else:
                try:
                    return jsonify(r.json()), r.status_code
                except:
                    return r.text, r.status_code

        headers = {}
        modified = validate_modified(request.headers.get("if-modified-since"))
        if modified:
            headers = {'if-modified-since': modified}

        params = {}
        if validate_list_query(request)[0]:
            params = request.args.to_dict()
        r = s.get(CONTACTS_URL + "/list", headers=headers, params=params)
        if r.status_code == 200:
            return jsonify(r.json())
        else:
            try:
                return jsonify(r.json()), r.status_code
            except:
                return r.text, r.status_code
    return "Not authorized", 500


if __name__ == '__main__':
    app.run(debug=True)
